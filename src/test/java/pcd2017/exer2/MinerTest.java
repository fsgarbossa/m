package pcd2017.exer2;

import static org.junit.Assert.assertTrue;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

public class MinerTest {

  /**
   * Root block
   */
  Block root = new Block(-2143436252,
      "42424280ada785bc0ee52dc3c7a626e27ea32d2d17a67b825b5ff13f71c3085b",
      new BlockData(
          "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef",
          "So long , and thanks for all the fish",
          timeStamp(2017, 12, 21, 14, 00)));

  /**
   * Data for the second block to be mined.
   */
  BlockData blockData = new BlockData(
      "42424280ada785bc0ee52dc3c7a626e27ea32d2d17a67b825b5ff13f71c3085b",
      "The Guide is definitive. Reality is frequently inaccurate.",
      timeStamp(2017, 12, 22, 14, 00));

  @Test
  public void testStream() throws InterruptedException, ExecutionException {
    System.out.println("'" + Miner.sha256(Miner.canonical(blockData, 1)) + "'");
    long start = System.currentTimeMillis();
    Block mine = new Miner().streamMine(blockData);
    long end = System.currentTimeMillis();
    System.out
        .println(mine.hash + " " + mine.nonce + " (" + (end - start) + ")");
  }

  @Test
  public void testThread() throws InterruptedException, ExecutionException {
    System.out.println("'" + Miner.sha256(Miner.canonical(blockData, 1)) + "'");
    long start = System.currentTimeMillis();
    Block mine = new Miner().threadMine(blockData);
    long end = System.currentTimeMillis();
    System.out
        .println(mine.hash + " " + mine.nonce + " (" + (end - start) + ")");
  }

  @Test
  public void validateRoot() {
    assertTrue(Miner.validate(root));
  }

  private long timeStamp(int year, int month, int day, int hour, int minute) {
    return ZonedDateTime
        .of(year, month, day, hour, minute, 0, 0, ZoneId.of("UTC")).toInstant()
        .getEpochSecond();
  }

}
